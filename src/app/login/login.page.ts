import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import firebase from 'firebase/app';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    public auth: AngularFireAuth,
    private route: Router,
    private navCtrl: NavController
  ) { }

  model = {
    username: "",
    password: ""
  }

  ngOnInit() {
    
  }

  doLogin() {
    this.auth.signInWithEmailAndPassword(this.model.username, this.model.password).then(auth => {
      localStorage.setItem('auth', JSON.stringify(auth))
      // JSON.parse(localStorage.auth)
      this.navCtrl.navigateRoot('/home');
      // this.route.navigateByUrl("/home")
    })
    .catch(error => {
      alert(JSON.stringify(error))
    })

    // this.auth.signInWithPopup(firebase.auth.GoogleAuthProvider()).then(data => {
    //   // this.navCtrl.navigateRoot('/home');
    //   // this.route.navigateByUrl("/home")
    //   // chamada da api ou service que chama a api e salva no banco
    // })
    // .catch(error => {
    //   alert(JSON.stringify(error))
    // })
  }

  signUp() {
    this.auth.createUserWithEmailAndPassword(this.model.username, this.model.password).then(auth => {
      console.log(auth);
    })
    .catch(error => {
      alert(JSON.stringify(error))
    })
  }

}
