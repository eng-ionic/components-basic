import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { PushService } from './shared/services/push.service';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

export const firebaseConfig = {
  apiKey: "AIzaSyBQ-lnrHf4Lftpzr-nybE3JCptjhbUS9Yg",
  authDomain: "meuapp-d61e5.firebaseapp.com",
  projectId: "meuapp-d61e5",
  storageBucket: "meuapp-d61e5.appspot.com",
  messagingSenderId: "581259713271",
  appId: "1:581259713271:web:5586d1a4641083524511f2",
  measurementId: "G-YZ3Z4Q47QL"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    OneSignal,
    PushService,
    AngularFireDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
