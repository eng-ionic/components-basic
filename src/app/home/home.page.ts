import { Component, OnInit } from '@angular/core';
import { MoneyFormatterPipe } from '../shared/pipes/money-formatter.pipe';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import { Device } from '@ionic-native/device/ngx';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [
    Camera,
    Device
  ]
})
export class HomePage implements OnInit {
  moneyFormatter = new MoneyFormatterPipe();
  base64Image: SafeResourceUrl;
  items:Array<any> = [];
  constructor(
    private camera: Camera,
    private sanitizer: DomSanitizer,
    private device: Device,
    private afDB: AngularFirestore
  ) {
    // alert(this.moneyFormatter.transform(1000, true));
    console.log('Device UUID is: ' + this.device.uuid);
  }

  ngOnInit(): void {
    alert('inited');
    this.afDB.collection('movies').valueChanges().subscribe(data => {
      console.log(data)
    })
  }
  
  movieClick(id) {
    alert(id);
  }

  takeAPick() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      saveToPhotoAlbum: true
    }
    
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     this.loadImage('data:image/jpeg;base64,' + imageData);
    }, (err) => {
     // Handle error
    });
  }

  loadImage(base64image: string){
    this.base64Image = this.sanitizer.bypassSecurityTrustResourceUrl(base64image);
  }
}
