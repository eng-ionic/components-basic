import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'moneyFormatter'
})
export class MoneyFormatterPipe implements PipeTransform {

  transform(value: any, useSign: boolean): string {
    let response = "";
    if (useSign) {
      response += "R$ ";
    }
    response += parseFloat(value).toFixed(2);
    return response;
  }

}
