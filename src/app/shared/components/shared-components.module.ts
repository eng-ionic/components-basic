import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieComponent } from './movie/movie.component';
import { IonicModule } from '@ionic/angular';
import { RelacionadosComponent } from './relacionados/relacionados.component';

@NgModule({
  declarations: [
    MovieComponent,
    RelacionadosComponent
  ],
  imports: [
    CommonModule,
    IonicModule.forRoot()
  ],
  exports: [
    MovieComponent,
    RelacionadosComponent
  ]
})
export class SharedComponentsModule { }
