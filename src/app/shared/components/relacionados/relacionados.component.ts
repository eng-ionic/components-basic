import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-relacionados',
  templateUrl: './relacionados.component.html',
  styleUrls: ['./relacionados.component.scss'],
})
export class RelacionadosComponent implements OnInit {
  @Output() movieClicked = new EventEmitter();

  movies = [
    {
      id: 1,
      title: "Meu titulo de Filme 1",
      subtitle: "sub 1",
      image: "https://image.tmdb.org/t/p/w440_and_h660_face/7CQuB71u4fbZzRARJMBLusFd5eL.jpg"
    },
    {
      id: 2,
      title: "Meu titulo de Filme 2",
      subtitle: "sub 2",
      image: "https://image.tmdb.org/t/p/w440_and_h660_face/mmRu2io9K21RioNDEWgE2eD88gR.jpg"
    }
  ]

  constructor() { }

  ngOnInit() {}

  movieClick(id) {
    this.movieClicked.emit(id);
  }

}
